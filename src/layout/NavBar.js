import Tab from "./Tab"
import { WEATHER_LOGO } from "../helpers/constans"

export default function NavBar() {

    return (
        <>
            <nav className="bg-white shadow">
                <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
                    <div className="relative flex h-16 justify-between">
                        <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
                            <div className="flex flex-shrink-0 items-center">
                                <img className="block h-8 w-auto lg:hidden" src={WEATHER_LOGO} alt="Weather" />
                                <img className="hidden h-8 w-auto lg:block" src={WEATHER_LOGO} alt="Weather" />
                            </div>
                            <div className="hidden sm:ml-6 sm:flex sm:space-x-8">
                                <Tab
                                    label='Current Weather'
                                    to={'/'}
                                />
                                <Tab
                                    label='Air Pollution'
                                    to={'/airPollution'}
                                />
                                <Tab
                                    label='Weather Forecast'
                                    to={'/weatherForecast'}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </nav >
        </>
    )

}
