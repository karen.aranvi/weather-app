import { WEATHER_IMAGE } from '../helpers/constans'

export default function WeatherCard({ type, data }) {
    return(
        <>    
            <div className="flex flex-col items-center mt-6 bg-white border border-gray-200 rounded-lg shadow md:flex-row md:max-w-xl">
            
                <img className="object-cover w-full rounded-t-lg h-96 md:h-auto md:w-48 md:rounded-none md:rounded-l-lg" src={WEATHER_IMAGE} alt="Weather" />

                <div className="flex flex-col justify-between p-4 leading-normal">
                    {type !== 'pollution' && 
                        <>
                            <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">{type === 'weather' ? `Current weather in ${data.city}` : `Weather forecast in ${data.city}`}</h5>
                            <ul className="list-disc ml-8">
                                {type === 'forecast' &&
                                    <li className="mb-3 font-normal text-gray-700">Time of data forecasted: {data.date} </li>
                                }
                                <li className="mb-3 font-normal text-gray-700">Weather condition: {data.description} </li>
                                <li className="mb-3 font-normal text-gray-700">Temperature: {data.temp} K</li>
                                <li className="mb-3 font-normal text-gray-700">Minimum temperature: {data.minTemp} K</li>
                                <li className="mb-3 font-normal text-gray-700">Maximum temperature:{data.maxTemp} K</li>
                                <li className="mb-3 font-normal text-gray-700">Feels like temperature: {data.feelsLikeTemp} K</li>
                                <li className="mb-3 font-normal text-gray-700">Humidity: {data.humidity} %</li>
                                <li className="mb-3 font-normal text-gray-700">Atmospheric pressure: {data.pressure} hPa</li>
                            </ul>
                        </>
                    }
                    {type === 'pollution' && 
                        <>
                            <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">Current air pollution in {data.city}</h5>
                            <ul className="list-disc ml-8">
                                <li className="mb-3 font-normal text-gray-700">Сoncentration of CO (Carbon monoxide): {data.co} μg/m3</li>
                                <li className="mb-3 font-normal text-gray-700">Сoncentration of NO (Nitrogen monoxide): {data.no} μg/m3</li>
                                <li className="mb-3 font-normal text-gray-700">Сoncentration of NO2 (Nitrogen dioxide): {data.no2} μg/m3</li>
                                <li className="mb-3 font-normal text-gray-700">Сoncentration of O3 (Ozone): {data.o3} μg/m3</li>
                                <li className="mb-3 font-normal text-gray-700">Сoncentration of SO2 (Sulphur dioxide): {data.so2} μg/m3</li>
                                <li className="mb-3 font-normal text-gray-700">Сoncentration of PM2.5 (Fine particles matter): {data.pm2_5} μg/m3</li>
                                <li className="mb-3 font-normal text-gray-700">Сoncentration of PM10 (Coarse particulate matter): {data.pm10} μg/m3</li>
                                <li className="mb-3 font-normal text-gray-700">Сoncentration of NH3 (Ammonia): {data.nh3} μg/m3</li>
                            </ul>
                        </>
                    }
                </div>
                
            </div>
        </>
    );
}