import { BrowserRouter, Routes, Route } from "react-router-dom"
import NavBar from '../layout/NavBar';
import HomeWeather from '../pages/HomeWeather';
import AirPollution from '../pages/AirPollution';
import WeatherForecast from '../pages/WeatherForecast';

export default function MainStack() {
	return (
        <>
            <BrowserRouter>
                <NavBar />
                <div className="flex flex-1 items-center justify-center mt-10 text-sky-500 font-bold text-3xl">
                    <h1>Weather App</h1>
                </div>

                <Routes>
                    <Route path={'/'} element={<HomeWeather />} />
                    <Route path={'/airPollution'} element={<AirPollution />} />
                    <Route path={'/weatherForecast'} element={<WeatherForecast />} />
                </Routes>

            </BrowserRouter>
        </>
    );
}