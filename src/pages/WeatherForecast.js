import { useState, useEffect } from "react"
import SearchCityInput from '../components/SearchCityInput';
import WeatherCard from '../components/WeatherCard'
import { getDailyForecast } from '../helpers/apiCalls';

export default function WeatherForecast() {
    const [city, setCity] = useState('')
    const [response, setResponse] = useState()
    const [forecast, setForecast] = useState([])

    useEffect(() => {
        if(response) {
            response.forEach((res) => {
                setForecast(forecast => [...forecast, {
                    city: city,
                    date: res.dt_txt,
                    description: res.weather[0].description,
                    temp: res.main.temp,
                    minTemp: res.main.temp_min,
                    maxTemp: res.main.temp_max,
                    feelsLikeTemp: res.main.feels_like,
                    humidity: res.main.humidity,
                    pressure: res.main.pressure
                }]);
            });
        }
    }, [response])

    const searchCityForecast = (e) => {
        getDailyForecast(city).then(res => {
            setResponse(res.list.slice(0, 16))      
        }).catch(err => {
            setForecast([])
        })
        e.preventDefault();
    }

    return (
        <>
           <div className="flex flex-row items-center justify-center mt-10">

                <div className="basis-1/5 mr-6">
                    <SearchCityInput 
                        type={'text'} 
                        id={'city'} 
                        value={city}
                        change={val => setCity(val)} 
                    />
                    
                </div>
                <div>
                    <button type="submit" onClick={searchCityForecast} className="text-white bg-sky-700 hover:bg-sky-500 focus:ring-4 focus:outline-none focus:ring-sky-500 font-medium rounded-lg text-lg px-4 py-2">
                        Search
                    </button>
                </div>
        
           </div>

           <div className="flex flex-1 items-center justify-center mt-10 font-bold text-2xl mt-10">
                <h1>2 day / 3 hour forecast data</h1>
            </div>

            {forecast.length > 0 &&
                <div className="flex flex-row basis-9/12 items-center justify-center" style={{display:'grid'}}>
                    {forecast.map(item => {
                        return <WeatherCard type='forecast' data={item} />
                    })}
                </div>
            }

        </>
    )
}
