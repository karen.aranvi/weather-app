import { useState, useEffect } from "react"
import SearchCityInput from '../components/SearchCityInput';
import WeatherCard from '../components/WeatherCard'
import { getAirPollution } from '../helpers/apiCalls';

export default function AirPollution() {
    const [city, setCity] = useState('')
    const [response, setResponse] = useState()
    const [currentAirPollution, setCurrentAirPollution] = useState({})

    useEffect(() => {
        if(response) {
            setCurrentAirPollution({
                city: city,
                co: response.components.co,
                nh3: response.components.nh3,
                no: response.components.no,
                no2: response.components.no2,
                o3: response.components.o3,
                pm2_5: response.components.pm2_5,
                pm10: response.components.pm10,
                so2: response.components.so2
            });

        }
    }, [response])

    const searchCityPollution = (e) => {
        getAirPollution(city).then(res => {
            setResponse(res.list[0])   
        }).catch(err => {
            setCurrentAirPollution({})
        })
        e.preventDefault();
    }

    return (
        <>
           <div className="flex flex-row items-center justify-center mt-10">

                <div className="basis-1/5 mr-6">
                    <SearchCityInput 
                        type={'text'} 
                        id={'city'} 
                        value={city}
                        change={val => setCity(val)} 
                    />
                    
                </div>
                <div>
                    <button type="submit" onClick={searchCityPollution} className="text-white bg-sky-700 hover:bg-sky-500 focus:ring-4 focus:outline-none focus:ring-sky-500 font-medium rounded-lg text-lg px-4 py-2">
                        Search
                    </button>
                </div>
        
           </div>

           {currentAirPollution.city &&
                <div className="flex flex-row basis-9/12 items-center justify-center mt-10">
                    <WeatherCard type='pollution' data={currentAirPollution} />
                </div>
            }
        </>
    )
}
