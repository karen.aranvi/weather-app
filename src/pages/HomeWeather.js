import { useState, useEffect } from "react"
import SearchCityInput from '../components/SearchCityInput';
import WeatherCard from '../components/WeatherCard';
import { getCurrentWeather } from '../helpers/apiCalls';

export default function HomeWeather() {
    const [city, setCity] = useState('');
    const [currentWeather, setCurrentWeather] = useState({});
    const [response, setResponse] = useState();

    useEffect(() => {
        if(response) {
            setCurrentWeather({
                city: city,
                description: response.weather[0].description,
                temp: response.main.temp,
                minTemp: response.main.temp_min,
                maxTemp: response.main.temp_max,
                feelsLikeTemp: response.main.feels_like,
                humidity: response.main.humidity,
                pressure: response.main.pressure
            });
        }
    }, [response]);

    const searchCity = (e) => {
        getCurrentWeather(city).then(res => {
            setResponse(res);
        }).catch(err => {
            setCurrentWeather({})
        })
        e.preventDefault();
    }

    return (
        <>
           <div className="flex flex-row items-center justify-center mt-10">

                <div className="basis-1/5 mr-6">
                    <SearchCityInput 
                        type={'text'} 
                        id={'city'} 
                        value={city}
                        change={val => setCity(val)} 
                    />
                    
                </div>
                <div>
                    <button type="submit" onClick={searchCity} className="text-white bg-sky-700 hover:bg-sky-500 focus:ring-4 focus:outline-none focus:ring-sky-500 font-medium rounded-lg text-lg px-4 py-2">
                        Search
                    </button>
                </div>
        
           </div>

           {currentWeather.city &&
                <div className="flex flex-row basis-9/12 items-center justify-center mt-10">
                    <WeatherCard type='weather' data={currentWeather} />
                </div>
            }

        </>
    )
}