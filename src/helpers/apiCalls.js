import axios from 'axios';

const WEATHER_MAP = 'https://api.openweathermap.org/data/2.5/weather?lat=';
const AIR_POLLUTION = 'http://api.openweathermap.org/data/2.5/air_pollution?lat=';
const WEATHER_FORECAST = 'https://api.openweathermap.org/data/2.5/forecast?lat=';
const GEO_WEATHER_MAP = 'https://api.openweathermap.org/geo/1.0/direct?q=';
const API_KEY = '6aab18c2a330bd7378e3329091f8ef1a';

export const getCurrentWeather = async (city) => {
    try {
        const res = await getGeocoding(city);
        return currentWeather(res.data[0].lat, res.data[0].lon);
    } catch (err) {
        throw(err);
    }
};

export const getAirPollution = async (city) => {
    try {
        const res = await getGeocoding(city);
        return airPollution(res.data[0].lat, res.data[0].lon);
    } catch (err) {
        throw(err);
    }
};

export const getDailyForecast = async (city) => {
    try {
        const res = await getGeocoding(city);
        return dailyForecastData(res.data[0].lat, res.data[0].lon);
    } catch (err) {
        throw(err);
    }
};

export const currentWeather = (lat, lon) => {
    return new Promise((resolve, reject) => {
        axios.get(`${WEATHER_MAP}${lat}&lon=${lon}&appid=${API_KEY}`)
            .then(res => {
                resolve(res.data);
            })
            .catch(err => reject(err));
    });
};

export const airPollution = (lat, lon) => {
    return new Promise((resolve, reject) => {
        axios.get(`${AIR_POLLUTION}${lat}&lon=${lon}&appid=${API_KEY}`)
            .then(res => {
                resolve(res.data);
            })
            .catch(err => reject(err));
    });
};

export const dailyForecastData = (lat, lon) => {
    return new Promise((resolve, reject) => {
        axios.get(`${WEATHER_FORECAST}${lat}&lon=${lon}&appid=${API_KEY}`)
            .then(res => {
                resolve(res.data);
            })
            .catch(err => reject(err));
    });
};

export const getGeocoding = (city) => {
    return new Promise((resolve, reject) => {
        axios.get(`${GEO_WEATHER_MAP}${city}&limit=1&appid=${API_KEY}`)
            .then(res => {
                resolve(res);
            })
            .catch(err => reject(err));
    });
};
